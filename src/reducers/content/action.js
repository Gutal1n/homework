import * as types from './types'
import { API_URL, API_VERSION, API_TOKEN } from 'config'
import { getIsNeedUpdate } from './selectors'

export const contentStart = () => ({
  type: types.CONTENT_START,
})

export const contentSuccess = payload => ({
  type: types.CONTENT_SUCCESS,
  payload,
  lastUpdate: Number(new Date()),
})

export const contentError = payload => ({
  type: types.CONTENT_ERROR,
  payload,
})

export const contentThunk = () => (dispatch, getState) => {
  if (getIsNeedUpdate(getState())) {
    dispatch(contentStart())
    return fetch(`${API_URL}/${API_VERSION}/content`, {
      headers: {
        Authorization: `Token ${API_TOKEN}`,
      },
    })
      .then(response => response.json())
      .then(response => dispatch(contentSuccess(response)))
      .catch(error => dispatch(contentError(error)))
  }
}
