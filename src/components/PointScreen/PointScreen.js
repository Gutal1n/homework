import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './PointScreen.css'

import PointHeaderImage from 'components/PointScreen/PointHeaderImage'
import Category from 'components/common/Category'
import TitlePoint from 'components/common/TitlePoint'
import Rating from 'components/common/Rating'
import PointPhoneNumber from 'components/PointScreen/PointPhoneNumber'

import { connect } from 'react-redux'
import { contentThunk } from '../../reducers/content/action'

// export default class PointScreen extends Component {
class PointScreen extends Component {
  static propTypes = {
    match: PropTypes.shape({
      params: PropTypes.object,
    }).isRequired,
    points: PropTypes.array.isRequired,
    categories: PropTypes.array.isRequired,
    contentThunk: PropTypes.func.isRequired,
  }

  getPoint = (array, indexPoint) => {
    for (let i = 0; i < array.length; i++) {
      if (array[i].id && array[i].id === indexPoint) {
        return array[i]
      }
    }
  }

  componentDidMount() {
    this.props.contentThunk()
  }

  render() {
    const index = parseInt(this.props.match.params.index, 10)
    const point = this.getPoint(this.props.points, index)
    const categories = this.props.categories
    // const categoriesList = point.category_id
    return (
      <div className="PointScreen">
        <PointHeaderImage photos={point.photos} />
        <div className="PointScreen__Rating">
          <Category categories={categories} categoriesList={point.category_id} />
          <Rating rating={point.rate} />
        </div>
        <TitlePoint title={point.name} />
        <div className="PointDescription">{point.description}</div>
        {point.description_2.length > 0 ? (
          <div className="PointWorkingHours">
            <b>Режим работы: </b>
            {point.description_2}
          </div>
        ) : (
          <div />
        )}
        {point.cost_sum.length > 0 ? (
          <div className="PointCostSum">
            <b>Стоимость посещения: </b>
            {point.cost_sum}
          </div>
        ) : (
          <div />
        )}
        <PointPhoneNumber phoneNumber={point.phone} />
      </div>
    )
  }
}

const mapStateToProps = state => {
  console.log('state', state)
  return {
    isFetching: state.isFetching,
    error: state.error,
    payload: state.payload,
    lastUpdate: state.lastUpdate,
    categories: state.content.payload.categories,
    points: state.content.payload.points,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    contentThunk: () => dispatch(contentThunk()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PointScreen)
