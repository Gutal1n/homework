import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './PointHeaderImage.css'
import { Link } from 'react-router-dom'

import backImg from '../../../assets/icBack.png'

export default class PointHeaderImage extends Component {
  static propTypes = {
    photos: PropTypes.array.isRequired,
  }

  render() {
    const { photos } = this.props
    const style = {
      backgroundImage: `url(${photos[0]})`,
    }
    return (
      <div className="PointHeaderImage" style={style}>
        <Link to={`/`} style={{ textDecoration: 'none' }}>
          <img className="PointHeaderImage__back" src={backImg} alt=" " />
        </Link>
      </div>
    )
  }
}
