import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './PointPhoneNumber.css'
import phoneLogo from '../../../assets/icPhone.png'

export default class PointPhoneNumber extends Component {
  static propTypes = {
    phoneNumber: PropTypes.string.isRequired,
  }

  render() {
    const { phoneNumber } = this.props
    if (phoneNumber === '') return null
    return (
      <div className="PointPhoneNumber">
        <img className="PointPhoneNumber__img" src={phoneLogo} alt=" " />
        <div className="PointPhoneNumber__div">{phoneNumber}</div>
      </div>
    )
  }
}
