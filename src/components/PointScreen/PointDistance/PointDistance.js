import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './PointDistance.css'
import locationImg from '../../../assets/icLocation'

export default class PointDistance extends Component {
  static propTypes = {
    distance: PropTypes.string.isRequired,
  }

  render() {
    const { phoneNumber } = this.props
    if (phoneNumber === null) return
    const phoneImg = '/assets/icPhone.png'
    return (
      <div className="PointDistance">
        <img className="PointDistance__img" src={locationImg} alt=" " />
        <div className="PointDistance__div">{phoneNumber}</div>
      </div>
    )
  }
}
