import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './HomeDescription.css'

export default class HomeDescription extends Component {
  static propTypes = {
    description: PropTypes.string.isRequired,
  }

  slice = text => {
    if (text.length > 50) text = text.slice(0, 50) + ' ...'
    return text
  }

  render() {
    let { description } = this.props
    return <div className="HomeDescription">{this.slice(description)}</div>
  }
}
