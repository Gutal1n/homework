import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './HomeModalFilter.css'
// import Category from 'components/common/Category'

export default class HomeModalFilter extends Component {
  static propTypes = {
    show: PropTypes.bool.isRequired,
    categories: PropTypes.array.isRequired,
  }

  render() {
    if (!this.props.show) {
      return null
    }
    console.log(this.props.categories)
    const categoriesList = this.props.categories.map((category, index) => {
      const style = {
        backgroundImage: `url(${category.icon})`,
      }
      return (
        <div className="HomeModalFilter__category" key={index}>
          <div className="HomeModalFilter__logo" style={style} />
          <div>{category.name}</div>
        </div>
      )
    })
    return <div className="HomeModalFilter"> {categoriesList} </div>
  }
}
