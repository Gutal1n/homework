import React, { Component } from 'react'
import PropTypes from 'prop-types'
import HomeCard from 'components/HomeScreen/HomeCard'
import './HomeScreen.css'

import { connect } from 'react-redux'
import { contentThunk } from '../../reducers/content/action'
import HomeModalFilter from './HomeModalFilter'

import isClose from '../../assets/icClose.png'
import logo from '../../assets/logoBlack.png'

// export default class HomeScreen extends Component {
class HomeScreen extends Component {
  static propTypes = {
    contentThunk: PropTypes.func.isRequired,
    isFetching: PropTypes.bool,
    error: PropTypes.bool,
    points: PropTypes.array.isRequired,
    categories: PropTypes.array.isRequired,
  }

  constructor(props) {
    super(props)

    this.state = { isOpen: false }
  }

  componentDidMount() {
    this.props.contentThunk()
  }

  toggleModal = () => {
    this.setState({
      isOpen: !this.state.isOpen,
    })
  }

  render() {
    const { isFetching, error, points, categories } = this.props
    if (isFetching) return <div>Loading...</div>
    if (error) return <div>Error!</div>
    let cards = points.map(point => {
      return <HomeCard point={point} categories={categories} key={point.id} />
    })
    const imgHeader = this.state.isOpen ? isClose : logo
    return (
      <div>
        <div className="HomeHeader" onClick={this.toggleModal}>
          <div className="HomeHeader__logo" style={{ backgroundImage: `url(${imgHeader})` }} />
          <div className="HomeHeader__filter">Фильтровать места</div>
        </div>
        <HomeModalFilter show={this.state.isOpen} categories={categories} />
        <div className="HomeScreen">{cards}</div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  console.log('state', state)
  return {
    isFetching: state.isFetching,
    error: state.error,
    payload: state.payload,
    lastUpdate: state.lastUpdate,
    categories: state.content.payload.categories,
    points: state.content.payload.points,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    contentThunk: () => dispatch(contentThunk()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)
