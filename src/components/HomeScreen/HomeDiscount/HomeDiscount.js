import React, { Component } from 'react'
import './HomeDiscount.css'
import PropTypes from 'prop-types'

export default class HomeDiscount extends Component {
  static propTypes = {
    discount: PropTypes.number.isRequired,
  }
  render() {
    const { discount } = this.props
    return discount > 0 ? (
      <div className="HomeDiscount">
        <span className="HomeDiscount__span">-{discount}%</span>
      </div>
    ) : (
      <div />
    )
  }
}
