import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './HomeCard.css'
import { Link } from 'react-router-dom'
import HomeDiscount from 'components/HomeScreen/HomeDiscount'
import Category from 'components/common/Category'
import TitlePoint from 'components/common/TitlePoint'
import HomeDescription from 'components/HomeScreen/HomeDescription'
import HomeDistance from 'components/HomeScreen/HomeDistance'

export default class HomeCard extends Component {
  static propTypes = {
    point: PropTypes.object.isRequired,
    categories: PropTypes.array.isRequired,
  }

  render() {
    const { categories, point } = this.props
    return (
      <div className="HomeCard">
        <Link to={`/point/${point.id}`} style={{ textDecoration: 'none' }}>
          <div className="HomeCard__header">
            <Category categories={categories} categoriesList={point.category_id} />
            <HomeDiscount discount={parseInt(point.discount_max, 10)} />
          </div>
          <TitlePoint title={point.name} />
          <HomeDescription description={point.description} />
          <HomeDistance latitude={point.latitude} longitude={point.longitude} />
        </Link>
      </div>
    )
  }
}
