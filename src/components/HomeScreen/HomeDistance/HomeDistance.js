import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './HomeDistance.css'

import locationImg from '../../../assets/icLocation.png'

export default class HomeDistance extends Component {
  static propTypes = {
    latitude: PropTypes.number.isRequired,
    longitude: PropTypes.number.isRequired,
  }

  calcDistance = (latitude, longitude) => {
    return '800 м'
  }

  render() {
    let { latitude, longitude } = this.props
    return (
      <div className="HomeDistance">
        <img className="HomeDistance__img" src={locationImg} alt=" " />
        <div className="HomeDistance__value">{this.calcDistance(latitude, longitude)}</div>
      </div>
    )
  }
}
