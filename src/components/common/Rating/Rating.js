import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './Rating.css'
import fullStar from '../../../assets/icStar.png'
import emptyStar from '../../../assets/icStarBorder.png'

export default class Rating extends Component {
  static propTypes = {
    rating: PropTypes.string.isRequired,
  }
  render() {
    let { rating } = this.props
    rating = parseInt(rating, 10)
    if (rating < 0 || rating > 5) return
    const stars = [1, 2, 3, 4, 5].map(element => {
      return element <= rating ? (
        <img className="Rating__img" src={fullStar} key={element} alt=" " />
      ) : (
        <img className="Rating__img" src={emptyStar} key={element} alt=" " />
      )
    })
    return <div className="Rating">{stars}</div>
  }
}
