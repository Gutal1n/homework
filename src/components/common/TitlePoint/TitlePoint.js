import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './TitlePoint.css'

export default class TitlePoint extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
  }
  render() {
    const { title } = this.props
    return <div className="TitlePoint">{title}</div>
  }
}
