import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './Category.css'

// Почему через img не отображается, а через div backgroundImage все нормально

export default class Category extends Component {
  static propTypes = {
    categories: PropTypes.array.isRequired,
    categoriesList: PropTypes.array.isRequired,
  }

  findCategoryById(categories, id) {
    for (let i = 0; i < categories.length; i++) {
      if (categories[i].id === id) return categories[i]
    }
  }

  render() {
    const { categories, categoriesList } = this.props
    if (categories.length === 0) return
    const list = categoriesList.map((categoryID, index) => {
      const category = this.findCategoryById(categories, categoryID)
      const styleDivImg = {
        backgroundImage: `url(${category.icon})`,
      }
      return (
        <div className="Category" key={index}>
          <div className="Category__divImg" style={styleDivImg} />
          {/* <img className="Category__img" scr={category.icon} alt={' '} /> */}
          <span className="Category__span">{category.name} </span>
        </div>
      )
    })
    return <div>{list}</div>
  }
}
